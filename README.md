# Docker image for Travis  CLI

Main use case is encrypting secrets (see
[Travis documentation](https://docs.travis-ci.com/user/encryption-keys/)). To use:

## Usage

Recommended usage is by means of the `travis.sh` script. You can also run the image
manually (see below).

Example:
```sh
curl -sL https://gitlab.com/CLARIN-ERIC/docker-travis-cli/raw/master/travis.sh | bash -s ~/my-git-repo encrypt my-secret
```

The above will run `travis encrypt my-secret` on the repository located in `~/my-git-repo`.

### Running the image

Mount the repository directory to `/data` and pass any arguments as a command.

Example:
```sh
docker run -v ~/my-git-repo encrypt:/data registry.gitlab.com/clarin-eric/docker-travis-cli:1.0.0 encrypt my-secret
```

## Install

You can 'install' Travis CLI locally by making a script: `/usr/local/bin/travis`:

```
#!/usr/bin/env bash
curl -sL https://gitlab.com/CLARIN-ERIC/docker-travis-cli/raw/master/travis.sh | bash -s . $@
```
