#!/bin/bash

TRAVIS_CLI_IMAGE="${TRAVIS_CLI_IMAGE:-registry.gitlab.com/clarin-eric/docker-travis-cli:1.0.0}"

main() {
	if [ "$1" == "-h" ] ||  [ "$1" == "-help" ]; then
		usage $@
		exit 0
	fi
	
	DIR="$1"
	shift

	ABSOLUTE_DIR="$(cd "$(dirname "$DIR")"; pwd -P)/$(basename "$DIR")"
	
	if [ -d "${ABSOLUTE_DIR}" ]; then
		docker run --rm -v "${ABSOLUTE_DIR}":/data "${TRAVIS_CLI_IMAGE}" $@
	else
		echo "Not a directory: ${ABSOLUTE_DIR}" >&2
		exit 1
	fi
	
}

usage() {
	echo "Usage:"
	echo "    $0 <dir> <args...>        Run 'travis <args...>' in specified directory"
	echo "    $0 <-h|--help>            Print this message"
}

main $@
